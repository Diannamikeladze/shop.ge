<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="style.css">
    
   
    <title>Shop.ge</title>
</head>
<body>
<style>
   .gan {
    display: flex;
    flex-wrap: wrap;
    justify-content: space-between;
    margin-bottom: 20px;
}

.gan > div {
    box-sizing: border-box;
    width: calc(50% - 10px);
    margin-bottom: 20px;
}

.gan span {
    display: block;
    margin-bottom: 5px;
    font-weight: bold;
}

.gan select,
.gan input {
    width: 100%;
    padding: 8px;
    margin-bottom: 10px;
    box-sizing: border-box;
}

.gan input[type="submit"] {
    background-color: #00597F;
    color: #fff;
    border: none;
    padding: 10px 20px;
    cursor: pointer;
    border-radius: 5px;
}

.gan input[type="submit"]:hover {
    background-color: #003f5c;
}

.gan a {
    text-decoration: none;
    color: white;
    font-weight: bold;
}


@media (max-width: 767px) {
    .gan > div {
        width: 100%;
    }
}

.footer {
  background-color: blue;
  color: #fff;
  padding: 20px 0;
}

.footer_container {
  max-width: 1200px;
  margin: 0 auto;
  display: flex;
  justify-content: space-between;
  align-items: center;
}

.footer_menu ul {
  list-style: none;
  padding: 0;
  margin: 0;
}

.footer_menu li {
  display: inline-block;
  margin-right: 20px;
}

.footer_menu a {
  text-decoration: none;
  color: #fff;
  font-weight: bold;
}

.location_main {
  font-size: 16px;
}

.location_main a {
  color: #fff;
  text-decoration: underline;
}

    
    .fashion_section {
    display: flex;
    flex-wrap: wrap;
    justify-content: space-between;
    margin-bottom: 20px;
}

.fashion_section > div {
    box-sizing: border-box;
    width: calc(33.33% - 20px);
    margin-bottom: 20px;
    background-color: #f5f5f5;
    padding: 20px;
}

.fashion_taital {
    font-size: 24px;
    margin-bottom: 10px;
}

.shirt_text {
    font-size: 18px;
    margin: 10px 0;
}

.price_text {
    font-size: 16px;
    margin-bottom: 10px;
}

.tshirt_img img,
.electronic_img img {
    max-width: 100%;
    height: auto;
}

.buy_bt a {
    text-decoration: none;
    padding: 10px 20px;
    background-color: #00597F;
    color: #fff;
    border-radius: 5px;
    display: inline-block;
}

.buy_bt a:hover {
    background-color: #003f5c;
}

@media (max-width: 767px) {
    .fashion_section > div {
        width: calc(50% - 20px);
    }
}

    .containe{
    width: 100%;
    padding: 20px;
    margin: 100px auto;
    background: #ddd;
    display: flex;
    flex-direction: row;
    justify-content: center;
}

.box{
    width: 250px;
   margin: 0 10px;
   box-shadow: 0 0 20px 2px rgba(0, 0, 0, .1);


}
.box img{
    display: block;
    width: 100%;
    border-radius: 5px;
    height: 100%;
}
.box:hover{
    transform: scale(1.3);

    z-index: 2;
    box-shadow: 2px 2px 2px #000;

    
}
.add img{
    display: block;
    width: 100%;
    border-radius: 5px; 
    height: 200px;
}
.navbar {
 width: 100%;
  margin: 0 auto;
  display: flex;
  justify-content: space-between;
  align-items: center;
  background-image: url('images/header-bg.jpg'); 
    background-size: cover; 
    background-position: center;
    background-repeat: no-repeat; 
}

.logo a {
  text-decoration: none;
  color: #fff;
  font-size: 24px;
  font-weight: bold;
}

.nav-links {
  list-style: none;
  padding: 0;
  margin: 0;
  display: flex;
  color: #000;
}

.nav-links li {
  margin-right: 20px;
}

.nav-links a {
  text-decoration: none;
  color:blue;
  font-weight: bold;
}


</style>
   
       <header >
        <nav class="navbar">
      <div class="logo">
        <a href="#"><img src="images/logo.png" alt="" srcset=""></a>
      </div>
      <ul class="nav-links">
        <li><a href="#">Home</a></li>
        <li><a href="#">Products</a></li>
        <li><a href="#">About Us</a></li>
        <li><a href="#">Contact</a></li>
        <li><a href="admin.php">Admin</a></li>
        <li><a href="registration.html">LogIn</a></li>
      </ul>
    </nav>
        <div class="page-header-mask">
       


    
   
    <div class="gan">
        <div >
            <span>მოძებნე განცხადება ან შეიძინე ონლაინ</span>
            <div>
                <select name="pills_gancxadebebi_quick">
                    <option value="gancxadebebi_quick" selected="selected">განცხადებები</option>
                    <option value="online_shop_quick">shop.ge - ონლაინ მაღაზია</option>
                </select>
            </div>
        </div>
    </div>
   
    <div >
        <div>
            <span>კატეგორია</span>
            <div>
                <input type="hidden" data-listing-type="gancxadebebi"  value=""/>
                <input type="hidden"  value=""/>
                <select>
                    <option value="0">- ყველა -</option>
                    <option value="172">ავეჯი, ინტერიერი</option>
                    <option value="4710">ავტომობილები, მოტო, სპეც-ტექნიკა</option>
                    <option value="129">აუდიო, ვიდეო, ფოტოტექნიკა</option>
                    <option value="268">ბიზნესი, დანადგარები</option>
                    <option value="61">გაქირავება</option>
                    
                </select>
            </div>
        </div>
  
    <div >
        <div>
            <span>ქალაქი/რაიონი</span>
            <div>
                
                <select >
                    <option >- ყველა -</option>
                    <option>თბილისი</option>
                    <option >ოზურგეთ</option>
                    <option >ქუთაისი</option>
                    <option>რუსთავი</option>
                    <option>გორი</option>
                    <option >მცხეთა</option>
                    <option>თელავი</option>
                    <option >ზუგდიდი</option>
                    <option >გურჯაანი</option>
                    <option >დედოფლისწყარო</option>
                    <option >დმანისი</option>
                    <option >დუშეთი</option>
                    <option>ვანი</option>
                    <option >ზესტაფონი</option>
                    <option >თეთრი წყარო</option>
                    <option >თერჯოლა</option>
                    <option >თიანეთი</option>
                    <option >კასპი</option>
                    <option >ლაგოდეხი</option>
                    <option >ლანჩხუთი</option>
                    <option >ლენტეხი</option>
                    <option >მარნეული</option>
                    
                </select>
            </div>
        </div>
    </div>

    <div >
        <div>
            <span>ფასი</span>
            <div>
                <input placeholder="-დან" class="numeric" type="text" name="f[price][from]" maxlength="15"/>
                <input placeholder="-მდე" class="numeric" type="text" name="f[price][to]" maxlength="15"/>
                <select title="Currency" name="f[price][currency]">
                    <option value="0">ყველა </option>
                    <option value="dollar">$</option>
                    <option value="euro">€</option>
                    <option value="currency_lari">₾</option>
                </select>
            </div>
        </div>
    </div>
    
        <div >
            <span></span>
            <div>
                <input type="submit" value="ძებნა"/>
                <a  title="დეტალური ძებნა" href="https://www.shop.ge/gancxadebebi/advanced-search.html"><h4 style="color: white ;">დეტალური ძებნა</h4></a>
            </div>
        
        </div>
    </div>
     
       </header>
       <div class="containe">
	<div class="box">
        
        <img src="images/256658362_large.jpg" alt="" srcset=""></div><br>
    <div class="box"><img src="images/toyota-prius-c-2128662738_large.jpg" alt="" srcset=""></div>

	<div class="box"><img src="images/h.jpg" alt="" srcset=""></div>
	<div class="box"><img src="images/glasses.png" alt="" srcset=""></div>
	<div class="box"><img src="images/yavis-aparati-1477416283_large.jpg" alt="" srcset=""></div>
</div>


<div class="fashion_section"  >
    
<div>
                                 <h4 class="shirt_text">უსაფრთოების კამერა</h4>
                                 <p class="price_text">ფასი  <span style="color: #262626;">500</span></p>
                                 <div class="tshirt_img"><img src="images/iyideba-usaprtkhk-1446417640_large.jpg"></div>
                           
                                  <a href="#">ყიდვა</a>
                                
                              
                           </div>
                           
                      <div>
                                 <h4 class="shirt_text">კარადა</h4>
                                 <p class="price_text">ფასი  <span style="color: #262626;">300</span></p>
                                 <div class="tshirt_img"><img src="images/karada-1706444888_large.png"></div>
                           
                                  <a href="#">ყიდვა</a>
                                
                              
                           </div>
                           
                      <div>
                                 <h4 class="shirt_text">ექსკავატორი</h4>
                                 <p class="price_text">ფასი  <span style="color: #262626;">3000</span></p>
                                 <div class="tshirt_img"><img src="images/car.jpg"></div>
                           
                                  <a href="#">ყიდვა</a>
                                
                              
                           </div>
         
                  
                      <div>
                                 <h4 class="shirt_text">მაისური</h4>
                                 <p class="price_text">ფასი  <span style="color: #262626;">30</span></p>
                                 <div class="tshirt_img"><img src="images/tshirt-img.png"></div>
                           
                                  <a href="#">ყიდვა</a>
                                
                              
                           </div>
                    <div>
                                 <h4 class="shirt_text">პერანგი</h4>
                                 <p class="price_text">ფასი  <span style="color: #262626;">50</span></p>
                                 <div class="tshirt_img"><img src="images/dress-shirt-img.png"></div>
                              
                                    <div class="buy_bt"><a href="#">ყიდვა</a></div>
                        
                           </div>
                           
                              <div>
                                 <h4 class="shirt_text">კაბა</h4>
                                 <p class="price_text">ფასი  <span style="color: #262626;">80</span></p>
                                 <div class="tshirt_img"><img src="images/women-clothes-img.png"></div>
                                 
                                    <div class="buy_bt"><a href="#">ყიდვა</a></div>
                                  
                                 </div>
                 
                        </div>
                     
     
      <div class="fashion_section">
         
       
                    
                     
                              <div>
                                 <h4 class="shirt_text">ლეპტოპი</h4>
                                 <p class="price_text">ფასი <span style="color: #262626;">800</span></p>
                                 <div class="electronic_img"><img src="images/laptop-img.png"></div>
                                 
                                    <div class="buy_bt"><a href="#">ყიდვა</a></div>
                                    
                           </div>
                          
                              
                              <div>
                                 <h4 class="shirt_text">ტელეფონი</h4>
                                 <p class="price_text">ფასი <span style="color: #262626;">1000</span></p>
                                 <div class="electronic_img"><img src="images/mobile-img.png"></div>
                                 
                                    <div class="buy_bt"><a href="#">ყიდვა</a></div>
                                    
                           </div>
                           
<div>
                                 <h4 class="shirt_text">კომპიუტერი</h4>
                                 <p class="price_text">ფასი <span style="color: #262626;">1000</span></p>
                                 <div class="electronic_img"><img src="images/computer-img.png"></div>
                                 
                                    <div class="buy_bt"><a href="#">ყიდვა</a></div>
                                    
                           </div>
                             
               </div>
             
               <footer class="footer">
    <div class="footer_container">
      <div class="footer_menu">
        <ul>
          <li><a href="#">Best Sellers</a></li>
          <li><a href="#">Gift Ideas</a></li>
          <li><a href="#">New Releases</a></li>
          <li><a href="#">Today's Deals</a></li>
          <li><a href="#">Customer Service</a></li>
        </ul>
      </div>
      <div class="location_main">
        Help Line Number : <a href="#">+995 1800 1200 1200</a>
      </div>
    </div>
  </footer>
      
      <script src="script.js"></script>

</body>
</html>
