<?php


function sanitizeInput($data) {
    $data = trim($data);
    $data = stripslashes($data);
    $data = htmlspecialchars($data);
    return $data;
}


function storeUserData($firstname, $lastname, $email, $password) {
    
    $message = "Registration successful! User: $firstname $lastname, Email: $email";
    return $message;
}

if ($_SERVER["REQUEST_METHOD"] == "POST") {
    if (isset($_POST['loginEmail']) && isset($_POST['loginPassword'])) {
        
        $loginEmail = sanitizeInput($_POST['loginEmail']);
        $loginPassword = sanitizeInput($_POST['loginPassword']);

        

        echo "Login successful!";
    } elseif (isset($_POST['regFirstname']) && isset($_POST['regLastname']) &&
              isset($_POST['regEmail']) && isset($_POST['regPassword'])) {
     
        $regFirstname = sanitizeInput($_POST['regFirstname']);
        $regLastname = sanitizeInput($_POST['regLastname']);
        $regEmail = sanitizeInput($_POST['regEmail']);
        $regPassword = sanitizeInput($_POST['regPassword']);

        $registrationMessage = storeUserData($regFirstname, $regLastname, $regEmail, $regPassword);
        echo $registrationMessage;
    } else {
        echo "Error: Invalid form data.";
    }
} else {
    echo "Error: Form not submitted.";
}

?>
