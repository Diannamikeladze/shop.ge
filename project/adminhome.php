<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>admin </title>
    <link rel="stylesheet" href="	https://cdn.jsdelivr.net/npm/bootstrap@5.3.2/dist/css/bootstrap.min.css">
</head>
<body>
    <div class="container my-5">
    <h2>List of clients</h2>
    <a class="btn btn-primary" href="create.php" role="button">New client</a>
    <br>
    <table class="table">
    <thead>
        <th>id</th>
        <th>name</th>
        <th>email</th>
        <th>phone</th>
        <th>adress</th>
        <th>product</th>
        <th>created at</th>
        <th>action</th>
        
    </thead>
    <tbody>
        <?php
        $servername = "localhost";
        $username = "root";
        $password = "";
        $database = "myshop";

        $connection = new mysqli($servername, $username, $password,$database);
        if ($connection->connect_error) {
            die("connection failed". $connection->connect_error);}

            $sql = "SELECT * FROM clients";
            $result = $connection->query($sql);
            
            while ($row = $result ->fetch_assoc()) {
                echo "
                <tr>
            <td>$row[id]</td>
            <td>$row[name]</td>
            <td>$row[email]</td>
            <td>$row[phone]</td>
            <td>$row[address]</td>
            <td>$row[product]</td>
            <td>$row[created_at]</td>
            <td>
            <a class='btn btn-primary btn-sm' href='/euphp2023/project/edit.php?=$row[id]' >edit</a>
            <a class='btn btn-danger btn-sm' href='/euphp2023/project/delete.php?=$row[id]' >delete</a>
            </td>
        </tr>
                ";
            }
        ?>
        
    </tbody>
    </table>
    </div>
</body>
</html>