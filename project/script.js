function validateLoginForm() {
    
    var email = document.getElementById('loginEmail').value;
    var password = document.getElementById('loginPassword').value;

    if (email === "" || password === "") {
        alert("Please enter both email and password.");
        return false;
    }


    return true;
}

function validateRegistrationForm() {
    
    var password = document.getElementById('regPassword').value;
    var confirmPassword = document.getElementById('regConfirmPassword').value;

    if (password !== confirmPassword) {
        alert("Passwords do not match");
        return false;
    }

    

    return true;
}



