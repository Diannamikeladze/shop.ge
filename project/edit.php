<?php 
 $servername = "localhost";
 $username = "root";
 $password = "";
 $database = "myshop";

 $connection = new mysqli($servername, $username, $password,
 $database);
$id = "";
$name = "";
$email = "";
$phone = "";
$address = "";
$product="";

$errorMessage = "";
$succesMessage = "";
 
if($_SERVER['REQUEST_METHOD'] == 'GET'){

    if(isset($_GET['id'])){
        header("location:/euphp2023/project/adminhome.php");
        exit;
    }
    $id = $_GET["id"];

    $sql= "SELECT * FROM clients WHERE id=$id";
    $result = $connection->query($sql);
    $row = $result->fetch_assoc();
    if($row){
        header("location:/euphp2023/project/adminhome.php");
        exit;
    }
    $name = $row ["name"];
    $email = $row ["email"];
    $phone = $row ["phone"];
    $address = $row ["address"];
    $product = $row ["product"];
}else{

    $id =$_POST["id"];
    $name = $_POST["name"];
    $email = $_POST["email"];
    $phone = $_POST["phone"];
    $address = $_POST["address"];
    $product = $_POST["product"];

    do {
        if(empty($id) || empty($name) || empty($email)
        || empty($phone)
    || empty($address)
    || empty($product)) 
    {
        $errorMessage = ("ყველა ველი სავალდებულოა");
        break;
    }
    $sql= "UPDATE clients". "SET name = '$name', email = '$email', phone = '$phone', address = '$address', product = '$product',".
    "WHERE id = $id";
    $resut = $connection->query($sql);
    if(!$result){
        $errorMessage = "არასწორია" . $connection->error;
        break;
    }
    $succesMessage = "კლიენტი წარმატებით შეიცვალა";
    header("location :/euphp2023/project/adminhome.php");
    exit;
    } while (true);
    
}


?>


<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
    <link rel="stylesheet" href="	https://cdn.jsdelivr.net/npm/bootstrap@5.3.2/dist/css/bootstrap.min.css">
    <script src="	https://cdn.jsdelivr.net/npm/bootstrap@5.3.2/dist/js/bootstrap.bundle.min.js"></script>
</head>
<body>
    <div class="container my-5">
    <h2>new client</h2>
    <?php
    if(!empty($errorMessage)){
        echo "
        <div class = 'alert alert-warning alert0dismissible fade show' role='alert'>
        <strong>$errorMessage</strong>
        <button type='button' class='btn-close' data-bs-dissmiss='alert' aria-label='close'></button>
        </div>
        ";
    }
    ?>

    
    <form method="post">
        <input type="hidden" name="id" value="<?php echo $id ;?>">
<div class="row mb-3">
    <label class="col-sm-3 col-form-label">Name</label>
    <div class="col-sm-6">
        <input type="text" class="form-control" name="name" value="<?php echo $name ;?>">
    </div>
    <div class="row mb-3">
    <label class="col-sm-3 col-form-label">email</label>
    <div class="col-sm-6">
        <input type="text" class="form-control" name="email" value="<?php echo $email ;?>">
    </div>
    <div class="row mb-3">
    <label class="col-sm-3 col-form-label">phone</label>
    <div class="col-sm-6">
        <input type="text" class="form-control" name="phone" value="<?php echo $phone ;?>">
    </div>
    <div class="row mb-3">
    <label class="col-sm-3 col-form-label">address</label>
    <div class="col-sm-6">
    <input type="text" class="form-control" name="address" value="<?php echo $address; ?>">
</div>

    <div class="row mb-3">
    <label class="col-sm-3 col-form-label">product</label>
    <div class="col-sm-6">
        <input type="text" class="form-control" name="product" value="<?php echo $product ;?>">
    </div>
</div>

<?php 
if (!empty($succesMessage)){
    echo "
    <div class = 'alert alert-warning alert0dismissible fade show' role='alert'>
    <strong>$succesMessage</strong>
    <button type='button' class='btn-close' data-bs-dissmiss='alert' aria-label='close'></button>
    </div>
    ";
}
?>

<div class="row mb-3">
    <div class="offset-sm-3 col-sm-3 d-grid">
        <button type="sumbit" class="btn btn-primary">Sumbit</button>
    </div>
    <div class="col-sm-3 d-grid">
        <a class="btn btn-outline-primary" href="adminhome.php" role="button">Cancel</a>
    </div>
    </div>
    </form>
    </div>
</body>
</html>